/**
 * Created by Rafal on 2015-04-17.
 */

(function(){
    var field = $("#field");
    var selected = $("#selected");
    var end = $("#end");
    var begin = $("#begin");


    var setOffset = function(tag){
        var heightToSet = (field.height() < tag.height())?(tag.height() - field.height())/2 : 0;
        tag.offset({top: field.offset().top - heightToSet, left: tag.offset().left - tag.width()/2});
    };
    setOffset(end);
    setOffset(begin);

    var showParagraph = $("#showValue");

    var getSize = function(point, tag){
        var maxSize = field.width() - begin.offset().left;
        var pointed = point;
        var size = pointed - begin.offset().left;
        if(size > maxSize) size = maxSize;
        if(size < 0) size = 0;
        tag.offset({left : pointed}); //Moves handle
        return size;
    }

    var getLeftSize = function(point, tag){
        var maxSize = end.offset().left - field.offset().left;
        var pointed = point - field.offset().left;
        var size = maxSize - pointed;
        if(size > maxSize) size = maxSize;
        if(size < 0) size = 0;

        selected.offset({left: pointed + field.offset().left});
        tag.offset({left: pointed + field.offset().left});
        return size;
    }

    var getValue = function(){
        return selected.width()/field.width();
    };


    $(begin).on('mousedown', function(event){
        selected.css("width", getLeftSize(event.pageX, begin));
        showParagraph.text((100 * getValue()).toFixed(4));
        $(window).on('mousemove', function(event){
            selected.css("width", getLeftSize(event.pageX, begin));
            showParagraph.text((100 * getValue()).toFixed(4));
        });
        $(window).on('mouseup', function(){
            $(window).off('mousemove');
        });
    });

    $(end).on('mousedown', function(event) {
        selected.css("width", getSize(event.pageX, end));
        showParagraph.text((100 * getValue()).toFixed(4));
        $(window).on('mousemove', function (event) {
            selected.css("width", getSize(event.pageX, end));
            showParagraph.text((100 * getValue()).toFixed(4));
        });
        $(window).on('mouseup', function () {
            $(window).off('mousemove');
        });
    });
})();
