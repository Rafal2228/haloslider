/**
 * Created by Rafal on 2015-04-12.
 */

$(function(){
    var photo,
        currentPos,
        maxPos,
        $slider = $('#slider');
    $.getJSON("other/photos.json")
        .done(function(data){
            photo = data.photo;
            //console.log(photo[0].url);
           // $('#slider').attr('src',photo[0].url);
        })
        .success(function(){
            $slider.attr('src',photo[0].url);
            currentPos = 0;
            maxPos = photo.length-1;
        });

    $("button").on('click', function(){
        if($slider.css('opacity') == 1){
            var direction = $(this).data("dir");

            if(photo !== null){
                currentPos += direction;
                if(currentPos > maxPos) currentPos = 0;
                if(currentPos < 0) currentPos = maxPos;

                $slider.css('opacity', 0);
                $slider.animate({opacity: 1}, 'slow');
                $slider.attr('src',photo[currentPos].url);
            }
        }
    });
});